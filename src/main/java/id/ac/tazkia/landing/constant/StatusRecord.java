package id.ac.tazkia.landing.constant;

public enum StatusRecord {
    ACTIVE, DONE, DELETED, FINISH
}

