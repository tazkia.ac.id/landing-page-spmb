package id.ac.tazkia.landing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SantriApplication {

	public static void main(String[] args) {
		SpringApplication.run(SantriApplication.class, args);
	}

}
