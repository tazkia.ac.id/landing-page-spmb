package id.ac.tazkia.landing.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity @Data
public class Subscribe {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull @NotEmpty
    private String nama;

    @NotNull @NotEmpty
    private String email;

    @NotNull @NotEmpty
    private String noWa;

    private String asalKota;

    @NotEmpty @NotEmpty
    private String asalSekolah;

    @NotEmpty @NotNull
    private String status;

    @NotNull
    private LocalDateTime tglInsert = LocalDateTime.now();

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_fu")
    private Followup followup;

    private LocalDateTime tglUpdate;

    @ManyToOne
    @JoinColumn(name = "user_update")
    private User userUpdate;

    private Integer hubungi;

    public String sumber;

    private  String kelas;

    private String tahunLulus;
    private String kelasSma;
    private String lulusan;

    @NotNull
    @OneToOne
    @JoinColumn(name = "id_tahunajaran")
    private TahunAjaran tahunAjaran;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Jenjang jenjang;

}
