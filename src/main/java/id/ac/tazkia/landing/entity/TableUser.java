package id.ac.tazkia.landing.entity;

import id.ac.tazkia.landing.constant.RecordStatus;
import id.ac.tazkia.landing.constant.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;


@Entity
@Table(name = "table_user")
@Data
public class TableUser {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String username;

    @Enumerated(EnumType.STRING)
    private RecordStatus active;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_role")
    private TableRole role;

    private LocalDateTime waktuMulai;

    private LocalDateTime waktuSelesai;

    private Integer reading;
    private Integer konversiReading;
    private Integer structure;
    private Integer konversiStructure;
    private Integer total;
    private Integer nilaiTotal;

    @Enumerated(EnumType.STRING)
    private StatusRecord status;

    @ManyToOne
    @JoinColumn(name = "id_subscribe")
    private Subscribe subscribe;

}
