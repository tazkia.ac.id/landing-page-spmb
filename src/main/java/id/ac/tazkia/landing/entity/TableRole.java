package id.ac.tazkia.landing.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "table_role")
@Data
public class TableRole {

        @Id
        @GeneratedValue(generator = "uuid" )
        @GenericGenerator(name = "uuid", strategy = "uuid2")
        private String id;
        private String name;
        private String description;

        @ManyToMany(fetch = FetchType.EAGER)
        @JoinTable(
                name = "table_role_permission",
                joinColumns = @JoinColumn(name = "id_role"),
                inverseJoinColumns = @JoinColumn(name = "id_permission")
        )
        private Set<TablePermision> permissions = new HashSet<>();




}
