package id.ac.tazkia.landing.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity @Data
public class Followup {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String nama;

    private String noWhatsapp;

    private String jumlah;

    @OneToOne
    @JoinColumn(name = "id_user")
    private User user;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Enumerated(EnumType.STRING)
    private Status s1;

    @Enumerated(EnumType.STRING)
    private Status s2;

    @Enumerated(EnumType.STRING)
    private Status kk;

    private String kategori;
}
