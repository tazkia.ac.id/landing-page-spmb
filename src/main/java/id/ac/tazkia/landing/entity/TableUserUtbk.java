package id.ac.tazkia.landing.entity;

import id.ac.tazkia.landing.constant.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class TableUserUtbk {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String username;

    @Enumerated(EnumType.STRING)
    private StatusRecord active;

    @ManyToOne
    @JoinColumn(name = "id_role")
    private TableRole role;

    @Column(columnDefinition = "DATETIME")
    private LocalDateTime waktuMulai;

    @Column(columnDefinition = "DATETIME")
    private LocalDateTime waktuSelesai;

    private Integer penalaranVariabel;

    private Integer penalaranKuantitatif;

    private Integer penalaranLogis;

    private Integer penalaranFigural;

    private Integer total;

    private Integer nilaiTotal;

    @Enumerated(EnumType.STRING)
    private StatusRecord status;

    private Integer saintek;

    private Integer soshum;

    private String statusPembayaran;

    @ManyToOne
    @JoinColumn(name = "id_subscribe")
    private Subscribe subscribe;

}
