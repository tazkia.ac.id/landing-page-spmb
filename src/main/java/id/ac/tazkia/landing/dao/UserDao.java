package id.ac.tazkia.landing.dao;

import id.ac.tazkia.landing.entity.TahunAjaran;
import id.ac.tazkia.landing.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserDao extends PagingAndSortingRepository<User, String> {
    User findByUsername(String username);
    Page<User> findByUsername(String username, Pageable pageable);

    @Query(value = "select u.username from s_user u inner join leads l on l.id_user = u.id where l.id_tahun = 0 and u.username = 1", nativeQuery = true)
    User findUsername (TahunAjaran tahunAjaran, String email);

}
