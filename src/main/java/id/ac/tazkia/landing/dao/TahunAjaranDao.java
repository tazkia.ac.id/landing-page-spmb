package id.ac.tazkia.landing.dao;

import id.ac.tazkia.landing.entity.Status;
import id.ac.tazkia.landing.entity.TahunAjaran;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TahunAjaranDao extends PagingAndSortingRepository<TahunAjaran,String> {

    TahunAjaran findByAktif(Status status);
    TahunAjaran findByKelas(String kelas);


}
