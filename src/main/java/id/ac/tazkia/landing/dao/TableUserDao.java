package id.ac.tazkia.landing.dao;

import id.ac.tazkia.landing.entity.TableRole;
import id.ac.tazkia.landing.entity.TableUser;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface TableUserDao extends PagingAndSortingRepository<TableUser,String> {

    Optional<TableUser> findByUsername(String username);

}
