package id.ac.tazkia.landing.dao;

import id.ac.tazkia.landing.entity.KabupatenKota;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface KabupatenKotaDao extends PagingAndSortingRepository<KabupatenKota, String> {
}
