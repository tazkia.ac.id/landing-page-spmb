package id.ac.tazkia.landing.dao;

import id.ac.tazkia.landing.entity.TableRole;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TableRoleDao extends PagingAndSortingRepository<TableRole,String> {
}
