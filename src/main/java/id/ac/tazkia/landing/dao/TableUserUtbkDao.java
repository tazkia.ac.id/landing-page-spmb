package id.ac.tazkia.landing.dao;

import id.ac.tazkia.landing.entity.TableUser;
import id.ac.tazkia.landing.entity.TableUserUtbk;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface TableUserUtbkDao extends PagingAndSortingRepository<TableUserUtbk, String> {

    Optional<TableUserUtbk> findByUsername(String username);

}
