package id.ac.tazkia.landing.controller;

import id.ac.tazkia.landing.constant.RecordStatus;
import id.ac.tazkia.landing.constant.StatusRecord;
import id.ac.tazkia.landing.dao.*;
import id.ac.tazkia.landing.entity.*;
import org.flywaydb.core.internal.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.time.LocalDateTime;
import java.util.Optional;

@Controller
public class LandingController {

    private static final String DOMAIN_KARYAWAN = "kelaskaryawan.tazkia.ac.id";
    private static final String DOMAIN_REGULER = "beasiswa.tazkia.ac.id";
    private static final String DOMAIN_PASCA = "pascasarjana.tazkia.ac.id";
    private static final Logger logger = LoggerFactory.getLogger(LandingController.class);
    @Autowired
    private SubscribeDao subscribeDao;
    @Autowired private FollowupDao followupDao;
    @Autowired private TahunAjaranDao tahunAjaranDao;
    @Autowired private KabupatenKotaDao kabupatenKotaDao;

    @Autowired private TableRoleDao tableRoleDao;
    @Autowired private TableUserDao tableUserDao;
    @Autowired private TableUserUtbkDao tableUserUtbkDao;

    @ModelAttribute("daftarKokab")
    public Iterable<KabupatenKota> daftarKokab(){
        return kabupatenKotaDao.findAll();
    }

    @GetMapping("/gagal")
    public void gagal(@RequestParam(value = "email", required = false) String email, Model model){
        if(email != null){
            model.addAttribute("email", email);
        }
    }
    @GetMapping("/selesai")
    public void selesai(@RequestParam(value = "email", required = false) String email, Model model){
        if(email != null){
            model.addAttribute("email", email);

        }
//
    }
    @GetMapping("/selesaiPameran")
    public void selesaiPameran(@RequestParam(value = "email", required = false) String email, Model model){
        if(email != null){
            model.addAttribute("email", email);

        }
//
    }
    @GetMapping("/selesaiKaryawan")
    public void selesaiKaryawan(@RequestParam(value = "email", required = false) String email, Model model) {
        if (email != null) {
            model.addAttribute("email", email);

        }
    }

    @GetMapping("/selesaiPasca")
    public void selesaiPasca(@RequestParam(value = "email", required = false) String email, Model model){
        if(email != null){
            model.addAttribute("email", email);

        }
    }

    @GetMapping("/sarjanaReguler")
    public String formIndex(@RequestParam(value = "utm_medium", required = false) String utm_medium, Model model){
        if (utm_medium != null) {
            model.addAttribute("utm_medium", utm_medium);
        }


        return "sarjanaReguler";
    }
//    @GetMapping("/")
//    public String form(@RequestParam(value = "utm_medium", required = false) String utm_medium, Model model){
//        if (utm_medium != null) {
//            model.addAttribute("utm_medium", utm_medium);
//        }
//
//
//        return "sarjanaReguler";
//    }

    @GetMapping("/")
    public String daftar(@RequestParam(value = "utm_medium", required = false) String utm_medium,
                         @RequestHeader(name="Host", required=false) final String host, Model model){
        if (utm_medium != null) {
            model.addAttribute("utm_medium", utm_medium);
        }

        logger.info("Hostname : {}", host);

        if(host.contains(DOMAIN_KARYAWAN)) {
            return "kelasKaryawan";
        } else if (host.contains(DOMAIN_REGULER)) {
            return "    sarjanaReguler";
        } else if (host.contains(DOMAIN_PASCA)) {
            return "pascasarjana";
        }
        throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "domain tidak dikenali"
        );
    }

// Get Mapping Load Artikel Kelas reguler
    @GetMapping("/s1-reguler")
    public void s1reguler(){}

    @GetMapping("/toefl")
    public String  toefl(){

         return "toeflbaru";
     }

    @PostMapping("/toefl")
    public String  prosesRegistrasiToefl(@RequestParam(value = "utm_medium", required = false) String utm_medium,
                                   Subscribe subscribe, RedirectAttributes attributes)  {

        Subscribe cekEmail = subscribeDao.findByEmail(subscribe.getEmail());

        if (cekEmail == null) {
            subscribe.setStatus("None");
            subscribe.setTglInsert(LocalDateTime.now());

            Followup followup = followupDao.cariUserFuS1();
            Integer jumlah = Integer.valueOf(followup.getJumlah());
            Integer jmlh = jumlah + 1;
            followup.setJumlah(jmlh.toString());
            followupDao.save(followup);
            logger.info("follow up by {}", followup.getNama());

            if (subscribe.getKelasSma().equals("Sudah Lulus") || subscribe.getKelasSma().equals("12")) {
                TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
                subscribe.setTahunAjaran(tahunAjaran);
            } else {
                TahunAjaran tahunAjaran = tahunAjaranDao.findByKelas(subscribe.getKelasSma());
                subscribe.setTahunAjaran(tahunAjaran);
            }
            subscribe.setFollowup(followup);
            subscribe.setHubungi(0);
            subscribe.setSumber(utm_medium);
            subscribe.setKelas("Reguler");
            subscribe.setJenjang(Jenjang.S1);
            subscribeDao.save(subscribe);

            TableUser tu = new TableUser();
            tu.setUsername(subscribe.getEmail());
            tu.setActive(RecordStatus.ACTIVE);
            TableRole tr = tableRoleDao.findById("peserta").get();
            tu.setRole(tr);
            tu.setSubscribe(subscribe);
            tableUserDao.save(tu);

            logger.info("Sukses : {} ", cekEmail);
            attributes.addFlashAttribute("selesai", "selesai");
            return "redirect:https://toefl.tazkia.ac.id";
        } else {
            cekEmail.setTglInsert(LocalDateTime.now());
            cekEmail.setSumber("Test-Toefl-Tazkia");
            subscribeDao.save(cekEmail);
            Optional<TableUser> cekUser = tableUserDao.findByUsername(cekEmail.getEmail());
            if (!cekUser.isPresent()) {
                TableUser tu = new TableUser();
                tu.setUsername(subscribe.getEmail());
                tu.setActive(RecordStatus.ACTIVE);
                TableRole tr = tableRoleDao.findById("peserta").get();
                tu.setRole(tr);
                tu.setSubscribe(subscribe);
                tableUserDao.save(tu);
            }
            logger.info("Gagal : {} ", cekEmail);
            return "redirect:https://toefl.tazkia.ac.id";
        }
    }

    @GetMapping("/utbk")
    public String newUtbk(){

        return "utbk";
    }

    @PostMapping("/utbk")
    public String saveUtbk(@RequestParam String source, Subscribe subscribe, RedirectAttributes attributes){

        Subscribe cekEmail = subscribeDao.findByEmail(subscribe.getEmail());

        if (cekEmail == null) {
            subscribe.setStatus("None");
            subscribe.setTglInsert(LocalDateTime.now());

            Followup followup = followupDao.cariUserFuS1();
            Integer jumlah = Integer.valueOf(followup.getJumlah());
            Integer jmlh = jumlah + 1;
            followup.setJumlah(jmlh.toString());
            followupDao.save(followup);
            logger.info("follow up by {}", followup.getNama());

            if (subscribe.getKelasSma().equals("Sudah Lulus") || subscribe.getKelasSma().equals("12")) {
                TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
                subscribe.setTahunAjaran(tahunAjaran);
            } else {
                TahunAjaran tahunAjaran = tahunAjaranDao.findByKelas(subscribe.getKelasSma());
                subscribe.setTahunAjaran(tahunAjaran);
            }
            subscribe.setFollowup(followup);
            subscribe.setHubungi(0);
            subscribe.setSumber(source);
            subscribe.setKelas("Reguler");
            subscribe.setJenjang(Jenjang.S1);
            subscribeDao.save(subscribe);

            TableUserUtbk tuu = new TableUserUtbk();
            tuu.setUsername(subscribe.getEmail());
            tuu.setActive(StatusRecord.ACTIVE);
            TableRole tr = tableRoleDao.findById("peserta").get();
            tuu.setRole(tr);
            tuu.setSubscribe(subscribe);
            tableUserUtbkDao.save(tuu);

            logger.info("Buat subcribe baru : {} ", cekEmail);
            attributes.addFlashAttribute("selesai", "selesai");
            return "redirect:https://tryout.tazkia.ac.id/login";
        } else {
            cekEmail.setTglInsert(LocalDateTime.now());
            cekEmail.setSumber("Test-UTBK-Tazkia");
            subscribeDao.save(cekEmail);
            Optional<TableUserUtbk> cekUser = tableUserUtbkDao.findByUsername(cekEmail.getEmail());
            if (!cekUser.isPresent()) {
                TableUserUtbk tuu = new TableUserUtbk();
                tuu.setUsername(subscribe.getEmail());
                tuu.setActive(StatusRecord.ACTIVE);
                TableRole tr = tableRoleDao.findById("peserta").get();
                tuu.setRole(tr);
                tuu.setSubscribe(subscribe);
                tableUserUtbkDao.save(tuu);
            }
            logger.info("Edit subcribe sudah ada : {} ", cekEmail);
            return "redirect:https://tryout.tazkia.ac.id/login";
        }
    }

    @GetMapping("/artikel-utama")
    public void artikelutama(){}
    
    @GetMapping("/pts-islami")
    public void ptsislami(){}

    @GetMapping("/utbk-2023")
    public void utbk(){}

    @GetMapping("/jalur-utbk")
    public void jalurutbk(){}

    @GetMapping("/beasiswa-utbk")
    public void beasiswautbk(){}

    @GetMapping("/hasil-utbk-2023")
    public void hasilutbk2023(){}

// Get Mapping Load Artikel Reguler Program Studi KPI
    @GetMapping("/dakwah")
    public void dakwah(){}

    @GetMapping("/media-islam")
    public void mediaislam(){}

    @GetMapping("/khutbah-jumat")
    public void kjumat(){}

    @GetMapping("/radio-islam")
    public void radioislam(){}

    @GetMapping("/penyiaran-keagamaan")
    public void penyiarankeagamaan(){}

    @GetMapping("/video-ceramah")
    public void videoceramah(){}

    @GetMapping("/komunikasi-islam")
    public void kislam(){}

    @GetMapping("/pesan-dakwah")
    public void pdakwah(){}

    @GetMapping("/nasyid-islam")
    public void nasyidislam(){}

    @GetMapping("/penyiaran-digital-islam")
    public void pdislam(){}


    @GetMapping("/fatwa")
    public void fatwa(){}

    @GetMapping("/media-sosial-islam")
    public void mediasosialislam(){}

    @GetMapping("/radio-dakwah")
    public void radiodakwah(){}

    @GetMapping("/publikasi-islam")
    public void pbislam(){}

// Get Mapping Load Artikel Reguler Program Studi HES
    @GetMapping("/ilmuhukum")
    public void ilmuhukum(){}

    @GetMapping("/hukum")
    public void hukum(){}

    @GetMapping("/teori-hukum")
    public void teorihukum(){}

    @GetMapping("/peraturan-hukum")
    public void peraturanhukum(){}

    @GetMapping("/hukum-publik")
    public void hukumpublik(){}

    @GetMapping("/hukum-privat")
    public void hukumprivat(){}

    @GetMapping("/hukum-internasional")
    public void hukuminternasional(){}

    @GetMapping("/hukum-kepailitan")
    public void hukumkepailitan(){}

    @GetMapping("/hukum-bisnis")
    public void hukumbisnis(){}

    @GetMapping("/hukum-lingkungan")
    public void hukumlingkungan(){}

    @GetMapping("/hukum-ham")
    public void hukumham(){}

    @GetMapping("/hukum-ketenagakerjaan")
    public void hukumketenagakerjaan(){}

    @GetMapping("/hukum-keluarga")
    public void hukumkeluarga(){}

    @GetMapping("/hukum-keimigrasian")
    public void hukumkeimigrasian(){}

    @GetMapping("/hukum-pidana")
    public void hukumpidana(){}

    @GetMapping("/hukum-perdata")
    public void hukumperdata(){}

    @GetMapping("/hukum-perusahaan")
    public void hukumperusahaan(){}

    @GetMapping("/hukum-teknologi-infomasi")
    public void hukumti(){}

    @GetMapping("/hukum-properti")
    public void hukumproperti(){}

    @GetMapping("/jurusan-hukum")
    public void jurusanhukum(){}

    @GetMapping("/sarjana-hukum")
    public void sarjanahukum(){}

    @GetMapping("/magister-hukum")
    public void magisterhukum(){}

    @GetMapping("/doktor-hukum")
    public void doktorhukum(){}

    @GetMapping("/kuliah-hukum")
    public void kuliahhukum(){}




// Get Mapping Load Artikel Kelas reguler Menuju Tazkia Digital 2025
    @GetMapping("/pembiayaan-syariah")
    public void pbisyariah(){}

    @GetMapping("/karakteristik-ekonomi-syariah")
    public void krksyariah(){}

    @GetMapping("/koperasi-syariah")
    public void kosikasyariah(){}

    @GetMapping("/kpr-syariah")
    public void kprsyariah(){}

    @GetMapping("/pasar-modal-syariah")
    public void pmsyariah(){}

    @GetMapping("/saham-syariah")
    public void sahamsyariah(){}

    @GetMapping("/suara-islam")
    public void suaraislam(){}

    @GetMapping("/berita-islam")
    public void beritaislam(){}

    @GetMapping("/filsafat-islam")
    public void filsafatislam(){}

    @GetMapping("/liputan-islam")
    public void liputanislam(){}

    @GetMapping("/mahkamah-syariah")
    public void mahkamahsyariah(){}

    @GetMapping("/aku-islam")
    public void akuislam(){}

    @GetMapping("/bisnis-digital")
    public void bisnisdigital(){}

    @GetMapping("/bisnis-plan")
    public void bisnisplan(){}

    @GetMapping("/internet-bisnis")
    public void internetbisnis(){}

    @GetMapping("/kuliah-internet")
    public void kuliahinternet(){}

    @GetMapping("/jurusan-bisnis-digital")
    public void jurusanbisnisdigital(){}

    @GetMapping("/jurusan-digital")
    public void jurusandigital(){}

    @GetMapping("/fintech-syariah")
    public void fintechsyariah(){}

    @GetMapping("/crowdfunding-syariah")
    public void crdsyariah(){}


    @GetMapping("/nilai-utbk")
    public void nilaiutbk(){}

    @GetMapping("/snbt-2023")
    public void snbt(){}

    @GetMapping("/jalur-mandiri-utbk")
    public void jalurmandiriutbk(){}

    @GetMapping("/kuliah-jalur-nilai-utbk")
    public void kuliahjalur(){}

    @GetMapping("/utbk-snbt-snbp-2023")
    public void utbksnbt2023(){}

    @GetMapping("/universitas-yang-menggunakan-nilai-utbk-untuk-jalur-mandiri-2023")
    public void univutbk2023(){}


    @GetMapping("/s1-reguler-pendidikan")
    public void s1regulerpendidikan(){}

    @GetMapping("/s1-akuntansi-syariah")
    public void s1akuntansisyariah(){}

    @GetMapping("/s1-ekonomi-syariah")
    public void s1ekonomisyariah(){}

    @GetMapping("/s1-hukum-ekonomi-syariah")
    public void s1hukumekonomisyariah(){}

    @GetMapping("/s1-pendidikan-ekonomi-syariah")
    public void s1pendidikanekonomisyariah(){}

    @GetMapping("/s1-manajemen-bisnis-syariah")
    public void s1manajemenbisnissyariah(){}


    @GetMapping("/s1-reguler-murah")
    public void s1regulermurah(){}

    @GetMapping("/s1-reguler-manajemen")
    public void s1regulermanajemen(){}

    @GetMapping("/s1-reguler-ilmu-komunikasi")
    public void s1regulerilmukomunikasi(){}


    @GetMapping("/s1-reguler-hukum")
    public void s1regulerhukum(){}

    @GetMapping("/s1-reguler-jakarta")
    public void s1regulerjakarta(){}

    @GetMapping("/s1-reguler-bandung")
    public void s1regulerbandung(){}

    @GetMapping("/s1-reguler-bekasi")
    public void s1regulerbekasi(){}

    @GetMapping("/s1-reguler-tangerang")
    public void s1regulertangerang(){}

    @GetMapping("/s1-di-bogor")
    public void s1dibogor(){}

    @GetMapping("/s1-data-science")
    public void s1datascience(){}

    @GetMapping("/s1-data-analyst")
    public void s1dataanalyst(){}

    @GetMapping("/s1-digital-marketing")
    public void s1digitalmarketing(){}

    @GetMapping("/daftar-kuliah-online")
    public void daftarkuliahonline(){}

    @GetMapping("/kuliah-online-gratis")
    public void kuliahonlinegratis(){}

    @GetMapping("/berapa-tahun-kuliah-s1")
    public void berapatahunkuliahs1(){}

    @GetMapping("/kampus-favorit")
    public void kampusfavorit(){}

    @GetMapping("/kip-kuliah")
    public void kipkuliah(){}

    @GetMapping("/kampus-dekat-puncak-bogor")
    public void kampusdekatpuncakbogor(){}

    @GetMapping("/snpmb-2023")
    public void snpmb2023(){}

    @GetMapping("/cyber-campus")
    public void cybercampus(){}

    @GetMapping("/kampus-swasta-bogor")
    public void kampusswastabogor(){}

    @GetMapping("/kampus-swasta-jakarta")
    public void kampusswastajkt(){}

    @GetMapping("/s1-reguler-depok")
    public void s1regulerdepok(){}

    @GetMapping("/s1-reguler-bogor")
    public void s1regulerbogor(){}

    @GetMapping("/s1-reguler-bogor-barat")
    public void s1regulerbogorbarat(){}

    @GetMapping("/s1-reguler-bogor-tengah")
    public void s1regulerbogortengah(){}

    @GetMapping("/s1-reguler-bogor-selatan")
    public void s1regulerbogorselatan(){}

    @GetMapping("/s1-reguler-bogor-utara")
    public void s1regulerbogorutara(){}

    @GetMapping("/s1-reguler-bogor-timur")
    public void s1regulerbogortimur(){}

    @GetMapping("/s1-reguler-sukabumi")
    public void s1regulersukabumi(){}

    @GetMapping("/s1-reguler-cimahi")
    public void s1regulercimahi(){}

    @GetMapping("/s1-reguler-cibubur")
    public void s1regulercibubur(){}

    @GetMapping("/s1-reguler-cirebon")
    public void s1regulercirebon(){}

    @GetMapping("/s1-reguler-kranggan")
    public void s1regulerkranggan(){}

    @GetMapping("/s1-reguler-cibinong")
    public void s1regulercibinong(){}

    @GetMapping("/s1-reguler-banjar")
    public void s1regulerbanjar(){}

    @GetMapping("/s1-reguler-karanganyar")
    public void s1regulerkaranganyar(){}

    @GetMapping("/s1-reguler-tanah-sareal")
    public void s1regulertanahsareal(){}

    @GetMapping("/s1-reguler-bandung-barat")
    public void s1regulerbandungbarat(){}

    @GetMapping("/s1-reguler-akuntansi")
    public void s1regulerakuntansi(){}

    @GetMapping("/3-tahun-jadi-guru")
    public void s13tahunguru(){}

    @GetMapping("/kuliah-study-trip")
    public void kuliahstudytrip(){}

    @GetMapping("/pertukaran-mahasiswa")
    public void ptmahasiswa(){}

    @GetMapping("/perbankan-syariah")
    public void pbsyariah(){}

    @GetMapping("/lulus-langsung-bisnis")
    public void lsbisnis(){}

    @GetMapping("/hibah-kemitraan")
    public void hbkemitraan(){}

    @GetMapping("/s1-talent-scouting")
    public void s1ts(){}

    @GetMapping("/kecerdasan-digital-syariah")
    public void kdsyariah(){}

    @GetMapping("/kesehatan-mental-islami")
    public void kmislami(){}

    @GetMapping("/kampus-online-offline")
    public void kmonline(){}

    @GetMapping("/kampus-anti-bully")
    public void kpbully(){}

    @GetMapping("/lolos-bumn")
    public void lolosbumn(){}

    @GetMapping("/kelas-reguler")
    public void kreguler(){}

    @GetMapping("/syariah-adalah")
    public void syariahadalah(){}

    @GetMapping("/fakultas-adalah")
    public void fakultasadalah(){}

    @GetMapping("/jurusan-ekonomi")
    public void jurusanekonomi(){}

    @GetMapping("/jurusan-ekonomi-syariah")
    public void jurusanekonomisyariah(){}

    @GetMapping("/jurusan-pendidikan-agama-islam")
    public void jurusanpai(){}

    @GetMapping("/smile-tazkia")
    public void smiletazkia(){}

    @GetMapping("/elearning-tazkia")
    public void elearningtazkia(){}


    @GetMapping("/ekonomi-islam")
    public void ekonomiislam(){}

    @GetMapping("/perguruan-tinggi")
    public void perguruantinggi(){}

    @GetMapping("/perguruan-tinggi-adalah")
    public void perguruantinggiadalah(){}

    @PostMapping("/sarjanaRegulerAds")
    public String  prosesSubscribeAds(@RequestParam(value = "utm_medium", required = false) String utm_medium,
                                      Subscribe subscribe, RedirectAttributes attributes)  {

        Subscribe cekEmail = subscribeDao.findByEmailOrNoWa(subscribe.getEmail(), subscribe.getNoWa());

        if (cekEmail == null) {
            subscribe.setStatus("None");
            subscribe.setTglInsert(LocalDateTime.now());

            if (utm_medium != null && !utm_medium.isEmpty()) {
                logger.info("Cek Utm {} ", utm_medium);
                String cekSumber = utm_medium.substring(0, 3);
                logger.info("Cek {} ", cekSumber);
                if (cekSumber.equals("BOO")) {
                    Followup followup = followupDao.cariUserFuS1Booth();
                    if (followup == null) {
                        followup = followupDao.cariUserFuS1Semua();
                    }
                    Integer jumlah = Integer.valueOf(followup.getJumlah());
                    Integer jmlh = jumlah + 1;
                    followup.setJumlah(jmlh.toString());
                    followupDao.save(followup);
                    logger.info("follow up by {}", followup.getNama());
                    subscribe.setFollowup(followup);

                }else if (cekSumber.equals("ADS")) {
                    Followup followup = followupDao.cariUserFuS1Ads();
                    if (followup == null) {
                        followup = followupDao.cariUserFuS1Semua();
                    }
                    Integer jumlah = Integer.valueOf(followup.getJumlah());
                    Integer jmlh = jumlah + 1;
                    followup.setJumlah(jmlh.toString());
                    followupDao.save(followup);
                    logger.info("follow up by {}", followup.getNama());
                    subscribe.setFollowup(followup);
                }else{
                    Followup followup = followupDao.cariUserFuS1Semua();
                    Integer jumlah = Integer.valueOf(followup.getJumlah());
                    Integer jmlh = (Integer) (jumlah + 1);
                    followup.setJumlah(jmlh.toString());
                    followupDao.save(followup);
                    logger.info("follow up by {}", followup.getNama());
                    subscribe.setFollowup(followup);
                }
            }else {
                Followup followup = followupDao.cariUserFuS1();
                Integer jumlah = Integer.valueOf(followup.getJumlah());
                Integer jmlh = jumlah + 1;
                followup.setJumlah(jmlh.toString());
                followupDao.save(followup);
                logger.info("follow up by {}", followup.getNama());
                subscribe.setFollowup(followup);

            }

            if (subscribe.getKelasSma().equals("Sudah Lulus") || subscribe.getKelasSma().equals("12")) {
                TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
                subscribe.setTahunAjaran(tahunAjaran);
            } else {
                TahunAjaran tahunAjaran = tahunAjaranDao.findByKelas(subscribe.getKelasSma());
                subscribe.setTahunAjaran(tahunAjaran);
            }
            subscribe.setHubungi(0);
            subscribe.setSumber(utm_medium);
            subscribe.setKelas("Reguler");
            subscribe.setJenjang(Jenjang.S1);
            subscribeDao.save(subscribe);

            logger.info("Sukses : {} ", cekEmail);
            attributes.addFlashAttribute("selesai", "selesai");
            return "redirect:/selesai?email="+subscribe.getEmail();
        } else {
            logger.info("Gagal : {} ", cekEmail);
            return "redirect:/gagal?email="+subscribe.getEmail();
        }
    }
    @PostMapping("/sarjanaReguler")
    public String  prosesSubscribe(@RequestParam(value = "utm_medium", required = false) String utm_medium,
                                  Subscribe subscribe, RedirectAttributes attributes)  {

        Subscribe cekEmail = subscribeDao.findByEmailOrNoWa(subscribe.getEmail(), subscribe.getNoWa());
        if (cekEmail == null) {
            subscribe.setStatus("None");
            subscribe.setTglInsert(LocalDateTime.now());

            Followup followup = followupDao.cariUserFuS1Semua();
            Integer jumlah = Integer.valueOf(followup.getJumlah());
            Integer jmlh = jumlah + 1;
            followup.setJumlah(jmlh.toString());
            followupDao.save(followup);
            logger.info("follow up by {}", followup.getNama());

            if (subscribe.getKelasSma().equals("Sudah Lulus") || subscribe.getKelasSma().equals("12")) {
                TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
                subscribe.setTahunAjaran(tahunAjaran);
            } else {
                TahunAjaran tahunAjaran = tahunAjaranDao.findByKelas(subscribe.getKelasSma());
                subscribe.setTahunAjaran(tahunAjaran);
            }
            subscribe.setHubungi(0);
            subscribe.setSumber(utm_medium);
            subscribe.setKelas("Reguler");
            subscribe.setJenjang(Jenjang.S1);
            subscribe.setFollowup(followup);
            subscribeDao.save(subscribe);

            logger.info("Sukses : {} ", cekEmail);
            attributes.addFlashAttribute("selesai", "selesai");
            return "redirect:/selesai?email="+subscribe.getEmail();
        } else {
            logger.info("Gagal : {} ", cekEmail);
            return "redirect:/gagal?email="+subscribe.getEmail();
        }
    }

    @GetMapping("/pameran")
    public String formPemaran(@RequestParam(value = "utm_medium", required = false) String utm_medium, Model model){
        if (utm_medium != null) {
            model.addAttribute("utm_medium", utm_medium);
        }
        return "pameran";
    }

    @PostMapping("/pameran")
    public String  prosesPameran(@RequestParam(value = "utm_medium", required = false) String utm_medium,
                                   Subscribe subscribe, RedirectAttributes attributes)  {

        Subscribe cekEmail = subscribeDao.findByEmailOrNoWa(subscribe.getEmail(), subscribe.getNoWa());

        if (cekEmail == null) {
            subscribe.setStatus("None");
            subscribe.setTglInsert(LocalDateTime.now());

            Followup followup = followupDao.cariUserFuS1();
            Integer jumlah = Integer.valueOf(followup.getJumlah());
            Integer jmlh = jumlah + 1;
            followup.setJumlah(jmlh.toString());
            followupDao.save(followup);
            logger.info("follow up by {}", followup.getNama());

            if (subscribe.getKelasSma().equals("Sudah Lulus") || subscribe.getKelasSma().equals("12")) {
                TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
                subscribe.setTahunAjaran(tahunAjaran);
            } else {
                TahunAjaran tahunAjaran = tahunAjaranDao.findByKelas(subscribe.getKelasSma());
                subscribe.setTahunAjaran(tahunAjaran);
            }
            subscribe.setFollowup(followup);
            subscribe.setHubungi(0);
            subscribe.setSumber(utm_medium);
            subscribe.setKelas("Reguler");
//            subscribe.setJenjang(Jenjang.S1);
            subscribeDao.save(subscribe);

            logger.info("Sukses : {} ", cekEmail);
            attributes.addFlashAttribute("selesai", "selesai");
            attributes.addFlashAttribute("utm_medium", utm_medium);
            return "redirect:/selesaiPameran?email="+subscribe.getEmail();
        } else {
            logger.info("Gagal : {} ", cekEmail);
            return "redirect:/gagal?email="+subscribe.getEmail();
        }
    }

    @GetMapping("/kelasKaryawan")
    public String formKelasKaryawan(@RequestParam(value = "utm_medium", required = false) String utm_medium, Model model){
        if (utm_medium != null) {
            model.addAttribute("utm_medium", utm_medium);
        }


        return "kelasKaryawan";
    }

// Get Mapping Load Artikel Kelas Karyawan
    @GetMapping("/s1-kelas-karyawan-murah-bogor")
    public void s1kkmurahbogor(){}

    @GetMapping("/kuliah-karyawan")
    public void kuliahkaryawan(){}

    @GetMapping("/s1-kelas-karyawan-manajemen-bisnis-murah")
    public void s1kkmanajemenbisnismurah(){}

    // Get Mapping Load Artikel Kelas reguler Menuju Tazkia Digital 2025
    @GetMapping("/kelas-eksekutif")
    public void kelaseksekutif(){}

    @GetMapping("/pendidikan-malam")
    public void pendidikanmalam(){}

    @GetMapping("/kuliah-fleksibel")
    public void kuliahfleksibel(){}

    @GetMapping("/paruh-waktu")
    public void paruhwaktu(){}

    @GetMapping("/jadwal-malam")
    public void jadwalmalam(){}

    @GetMapping("/pengalaman-kerja")
    public void pkerja(){}

    @GetMapping("/waktu-luang")
    public void waktuluang(){}

    @GetMapping("/kesempatan-karir")
    public void kkarir(){}

    @GetMapping("/kesempatan-kuliah")
    public void kkuliah(){}

    @GetMapping("/sertifikat-kerja")
    public void skerja(){}

    @GetMapping("/waktu-belajar")
    public void wbelajar(){}

    @GetMapping("/wawasan-industri")
    public void windustri(){}

    @GetMapping("/karyawan-swasta")
    public void kswasta(){}

    @GetMapping("/usaha-sampingan")
    public void usahasampingan(){}

    @GetMapping("/umr-karyawan")
    public void umrkaryawan(){}

    @GetMapping("/tenaga-pendidikan")
    public void tenagapendidikan(){}

    @GetMapping("/skkm-kuliah")
    public void skkmkuliah(){}

    @GetMapping("/skp-kuliah")
    public void skpkuliah(){}

    @GetMapping("/motivasi-kuliah")
    public void motivkuliah(){}

    @GetMapping("/pengembangan-profesional")
    public void pprofesional(){}

    @GetMapping("/bantuan-karyawan")
    public void bantuankaryawan(){}

    @GetMapping("/mahasiswa-karyawan")
    public void mahasiswakaryawan(){}

    @GetMapping("/program-karyawan")
    public void programkaryawan(){}

    @GetMapping("/kuliah-umum")
    public void kuliahumum(){}

    @GetMapping("/uang-pangkal")
    public void uangpangkal(){}

    @GetMapping("/sukses-karir")
    public void sukseskarir(){}

    @GetMapping("/prospek-karir")
    public void prospekkarir(){}


// Get Mapping Load Artikel Kompetitor Kelas Karyawan
    @GetMapping("/kelas-karyawan-esa-unggul")
    public void kkesaunggul(){}

    @GetMapping("/kelas-karyawan-bsi")
    public void kkbsi(){}

    @GetMapping("/kelas-karyawan-nusa-mandiri")
    public void kknusamandiri(){}

    @GetMapping("/kelas-karyawan-lp3i")
    public void kklp3i(){}

    @GetMapping("/kelas-karyawan-universitas-pakuan")
    public void kkunpak(){}

    @GetMapping("/kelas-karyawan-unbin")
    public void kkunbin(){}

    @GetMapping("/kelas-karyawan-ipb")
    public void kkipb(){}

    @GetMapping("/kelas-karyawan-universitas-nusa-bangsa")
    public void kknusabangsa(){}

    @GetMapping("/kelas-karyawan-politeknik-aka-bogor")
    public void kkpoltekbogor(){}

    @GetMapping("/kelas-karyawan-universitas-terbuka")
    public void kkut(){}

    @GetMapping("/kelas-karyawan-universitas-mercu-buana")
    public void kkum(){}

    @GetMapping("/kelas-karyawan-universitas-paramadina")
    public void kkparamadina(){}

    @GetMapping("/kelas-karyawan-universitas-islam-as-syafiiyah")
    public void kkuias(){}

    @GetMapping("/kelas-karyawan-universitas-pembangunan-jaya")
    public void kkupj(){}

    @GetMapping("/kelas-karyawan-binus-bina-nusantara")
    public void kkbinus(){}

    @GetMapping("/kelas-karyawan-uika")
    public void kkuika(){}

    @GetMapping("/universitas-djuanda")
    public void univdjuanda(){}

    @GetMapping("/sekolah-tinggi-pariwisata-bogor")
    public void stpbogor(){}

    @GetMapping("/institut-pertanian-bogor")
    public void ipbogor(){}

    @GetMapping("/unb-universitas-nusa-bangsa")
    public void unbangsa(){}

    @GetMapping("/stikom-el-rahma")
    public void stikomelrahma(){}

    @GetMapping("/sttif-bogor")
    public void sttifbogor(){}

    @GetMapping("/stih-dharma-andhiga")
    public void stihdharmaandhiga(){}

    @GetMapping("/trisakti-bogor")
    public void trisaktibogor(){}

    @GetMapping("/ibik-bogor")
    public void ibikbogor(){}

    @GetMapping("/ipi-garut")
    public void ipigarut(){}

    @GetMapping("/unindra-pgri")
    public void unindrapgri(){}

    @GetMapping("/unisa-yogya")
    public void unisayogya(){}

    @GetMapping("/uin-syarif-hidayatullah")
    public void uinsya(){}

    @GetMapping("/uin-sunan-kalijaga")
    public void uinsunankalijaga(){}

    @GetMapping("/uin-sunan-gunungdjati")
    public void uinsunangunungdjati(){}




    @GetMapping("/s1-kelas-karyawan-jakarta-manajemen-komunikasi")
    public void s1kkjakartamanajemenkomunikasi(){}

    @GetMapping("/s1-kelas-karyawan-jakarta-manajemen-bisnis-beasiswa")
    public void s1kkjakartamanajemenbisnisbeasiswa(){}

    @GetMapping("/s1-kelas-karyawan-jakarta-ilmu-komunikasi-beasiswa")
    public void s1kkjakartailmukomunikasibeasiswa(){}

    @GetMapping("/kelas-karyawan-di-bogor")
    public void kkdibogor(){}

    @GetMapping("/kelas-sore")
    public void kelassore(){}

    @GetMapping("/kuliah-sore")
    public void kuliahsore(){}

    @GetMapping("/kelas-malam")
    public void kelasmalam(){}

    @GetMapping("/kuliah-malam")
    public void kuliahmalam(){}

    @GetMapping("/kelas-karyawan-di-depok")
    public void kkdidepok(){}

    @GetMapping("/kelas-karyawan-di-sempur")
    public void kkdisempur(){}

    @GetMapping("/kelas-karyawan-blended-learning")
    public void kkblendedlearning(){}

    @GetMapping("/kelas-karyawan-pajak")
    public void kkpajak(){}

    @GetMapping("/kelas-karyawan-investasi")
    public void kkinvestasi(){}

    @GetMapping("/kelas-karyawan-keuangan")
    public void kkkeuangan(){}


    @GetMapping("/s1-kelas-karyawan-depok")
    public void s1kkdepok(){}

    @GetMapping("/s1-kelas-karyawan-tangerang")
    public void s1kktangerang(){}

    @GetMapping("/s1-kelas-karyawan-bekasi")
    public void s1kkbekasi(){}

    @GetMapping("/s1-kelas-karyawan-bandung")
    public void s1kkbandung(){}

    @GetMapping("/s1-kelas-karyawan-bogor")
    public void s1kkbogor(){}

    @GetMapping("/s1-kelas-karyawan-bogor-timur")
    public void s1kkbogortimur(){}

    @GetMapping("/s1-kelas-karyawan-bogor-barat")
    public void s1kkbogorbarat(){}

    @GetMapping("/s1-kelas-karyawan-bogor-utara")
    public void s1kkbogorutara(){}

    @GetMapping("/s1-kelas-karyawan-bogor-tengah")
    public void s1kkbogortengah(){}

    @GetMapping("/s1-kelas-karyawan-bogor-selatan")
    public void s1kkbogorselatan(){}

    @GetMapping("/s1-kelas-karyawan-jakarta")
    public void s1kkjakarta(){}

    @GetMapping("/s1-kelas-karyawan-banjar")
    public void s1kkbanjar(){}

    @GetMapping("/s1-kelas-karyawan-cibinong")
    public void s1kkcibinong(){}

    @GetMapping("/s1-kelas-karyawan-cimahi")
    public void s1kkcimahi(){}

    @GetMapping("/s1-kelas-karyawan-sukabumi")
    public void s1kksukabumi(){}

    @GetMapping("/s1-kelas-karyawan-tanah-sareal")
    public void s1kktanahsareal(){}

    @GetMapping("/s1-kelas-karyawan-karanganyar")
    public void s1kkkaranganyar(){}

    @GetMapping("/s1-kelas-karyawan-cirebon")
    public void s1kkcirebon(){}

    @GetMapping("/s1-kelas-karyawan-bandung-barat")
    public void s1kkbandungbarat(){}

    @GetMapping("/kelas-karyawan-malam")
    public void kkmalam(){}

    @GetMapping("/kelas-karyawan-sore")
    public void kksore(){}

    

    @GetMapping("/s1-kelas-karyawan-cepat-lulus-murah-depok")
    public void s1kkcepatlulusmurahdepok(){}

    @GetMapping("/s1-kelas-karyawan-bogor-komunikasi-murah")
    public void s1kkbogorkomunikasimurah(){}

    @GetMapping("/s1-kelas-karyawan-bogor-akuntansi-murah")
    public void s1kkbogorakuntansimurah(){}


    @GetMapping("/program-studi-kelas-karyawan")
    public void pskelaskaryawan(){}

    @GetMapping("/program-studi-kelas-karyawan-sore")
    public void pskelaskaryawansore(){}

    @GetMapping("/program-studi-kelas-karyawan-malam")
    public void pskelaskaryawanmalam(){}

    @GetMapping("/program-studi-kelas-karyawan-online")
    public void pskelaskaryawanonline(){}

    @GetMapping("/program-studi-kelas-karyawan-asal-sma")
    public void pskelaskaryawanasalsma(){}

    @GetMapping("/program-studi-kelas-karyawan-paket-c")
    public void pskelaskaryawanpaketc(){}



    @PostMapping("/kelasKaryawan")
    public String kelasKaryawan(@RequestParam(value = "utm_medium", required = false) String utm_medium,
                                Subscribe subscribe, RedirectAttributes attributes)  {
        Subscribe cekEmail = subscribeDao.findByEmailOrNoWa(subscribe.getEmail(), subscribe.getNoWa());

        if (cekEmail == null) {
            subscribe.setStatus("None");
            subscribe.setTglInsert(LocalDateTime.now());

            Followup followup = followupDao.cariUserFuKk();
            Integer jumlah = Integer.valueOf(followup.getJumlah());
            Integer jmlh = jumlah + 1;
            followup.setJumlah(jmlh.toString());
            followupDao.save(followup);
            logger.info("follow up by {}", followup.getNama());

            TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);

            subscribe.setTahunAjaran(tahunAjaran);
            subscribe.setFollowup(followup);
            subscribe.setHubungi(0);
            subscribe.setSumber(utm_medium);
            subscribe.setKelas("Karyawan");
            subscribe.setJenjang(Jenjang.S1);
            subscribeDao.save(subscribe);

            logger.info("Sukses : {} ", cekEmail);
            attributes.addFlashAttribute("selesai", "selesai");
            return "redirect:/selesaiKaryawan?email="+subscribe.getEmail();
        } else {
            logger.info("Gagal : {} ", cekEmail);
            return "redirect:/gagal?email="+subscribe.getEmail();
        }
    }

    @GetMapping("/pascasarjana")
    public String formPascasarjana(@RequestParam(value = "utm_medium", required = false) String utm_medium, Model model){
        if (utm_medium != null) {
            model.addAttribute("utm_medium", utm_medium);
        }

        return "pascasarjana";
    }

    // Get Mapping Load Artikel Pascasarjana
    @GetMapping("/s2-pascasarjana-murah-bogor")
    public void s2pascamurahbogor(){}

    @GetMapping("/s2-pascasarjana-magister-kuliah-online")
    public void s2pascasarjanamagisterkuliahonline(){}


    @GetMapping("/s2-kelas-karyawan-cepat-lulus-murah")
    public void s2karyawancepatlulusmurah(){}

    @GetMapping("/s2-kelas-karyawan-bogor")
    public void s2kelaskaryawanbogor(){}

    @GetMapping("/s2-magister-pascasarjana-gratis-uang-pangkal")
    public void s2gratisuangpangkal(){}


    @GetMapping("/s2-fleksibel")
    public void s2fleksibel(){}

    @GetMapping("/s2-online")
    public void s2online(){}

    @GetMapping("/kuliah-s2-online")
    public void kuliahs2online(){}

    @GetMapping("/s2-kelas-malam")
    public void s2kelasmalam(){}

    @GetMapping("/s2-kelas-sore")
    public void s2kelassore(){}

    @GetMapping("/s2-tugas-riset")
    public void s2tugasriset(){}

    @GetMapping("/riset-s2")
    public void risets2(){}

    @GetMapping("/penelitian-s2")
    public void penelitians2(){}

    @GetMapping("/s2-praktisi")
    public void s2praktisi(){}

    @GetMapping("/s2-akademisi")
    public void s2akademisi(){}

    @GetMapping("/s2-pascasarjana-magister-kuliah-fleksibel")
    public void s2magisterfleksibel(){}

    @GetMapping("/s2-ekonomi-gratis")
    public void s2ekonomigratis(){}

    @GetMapping("/s2-pascasarjana-magister-biaya-daftar-gratis")
    public void s2pascasarjanamagisterbiayagratis(){}

    @GetMapping("/s2-pascasarjana-magister-akuntansi-gratis")
    public void s2pascamagisterakuntansigratis(){}

    @GetMapping("/s2-pascasarjana-magister-ekonomi-gratis")
    public void s2pascamagisterekonomigratis(){}


    @GetMapping("/s2-magister-pascasarjana-kelas-lulus-1-5-tahun")
    public void s2magister1tahun(){}


    @GetMapping("/gelar-akademik-s2")
    public void gelarakademiks2(){}

    @GetMapping("/seleksi-penerimaan-s2")
    public void seleksipenerimaans2(){}

    @GetMapping("/pengalaman-belajar-s2")
    public void pengalamanbelajars2(){}

    @GetMapping("/karir-setelah-s2")
    public void karirsetelahs2(){}

    @GetMapping("/apa-itu-pascasarjana")
    public void apaitupascasarjana(){}

    @GetMapping("/kuliah-s2-sambil-kerja")
    public void kuliahs2sambilkerja(){}

    @GetMapping("/praktisi-dan-akademisi-s2")
    public void praktisis2(){}

    @GetMapping("/s2-akuntansi")
    public void s2akuntansi(){}

    @GetMapping("/s2-manajemen")
    public void s2manajemen(){}

    @GetMapping("/s2-di-bogor")
    public void s2dibogor(){}

    @GetMapping("/gelar-magister")
    public void gelarmagister(){}

    @GetMapping("/tesis-s2")
    public void tesiss2(){}

    @GetMapping("/mahasiswa-s2")
    public void mahasiswas2(){}

    @GetMapping("/jurusan-s2")
    public void jurusans2(){}

    @GetMapping("/beasiswa-s2")
    public void beasiswas2(){}

    @GetMapping("/beasiswa-s2-bogor")
    public void beasiswas2bogor(){}

    @GetMapping("/s2-pendidikan")
    public void s2pendidikan(){}

    @GetMapping("/beasiswa-s2-indonesia")
    public void beasiswas2indo(){}

    @GetMapping("/beasiswa-s2-dalam-negeri")
    public void beasiswas2negeri(){}

    @GetMapping("/beasiswa-s2-kampus-swasta")
    public void beasiswas2kampusswasta(){}

    @GetMapping("/kuliah-s2-berapa-semester")
    public void kuliahs2berapasemester(){}

    @GetMapping("/konsentrasi-s2")
    public void konsentrasis2(){}

    @GetMapping("/kurikulum-s2")
    public void kurikulums2(){}

    @GetMapping("/kompetensi-s2")
    public void kompetensis2(){}

    @GetMapping("/gelar-pascasarjana-tingkat-dua")
    public void gelarpascatingkat(){}

    @GetMapping("/program-pascasarjana-tingkat-lanjut")
    public void programpascasarjanalanjut(){}



    

    @GetMapping("/pascasarjana-akuntansi")
    public void pascasarjanaakuntansi(){}

    @GetMapping("/pascasarjana-manajemen")
    public void pascasarjanamanajemen(){}

    @GetMapping("/magister-akuntansi")
    public void magisterakuntansi(){}

    @GetMapping("/magister-manajemen")
    public void magistermanajemen(){}

    @GetMapping("/magister-akuntansi-syariah")
    public void magisterakuntansisyariah(){}

    @GetMapping("/magister-manajemen-syariah")
    public void magistermanajemensyariah(){}




    @PostMapping("/pascasarjana")
    public String pascasarjana(@RequestParam(value = "utm_medium", required = false) String utm_medium,
                                Subscribe subscribe, RedirectAttributes attributes)  {
        Subscribe cekEmail = subscribeDao.findByEmail(subscribe.getEmail());

        if (cekEmail == null) {
            subscribe.setStatus("None");
            subscribe.setTglInsert(LocalDateTime.now());

            Followup followup = followupDao.cariUserFuS2();
            Integer jumlah = Integer.valueOf(followup.getJumlah());
            Integer jmlh = jumlah + 1;
            followup.setJumlah(jmlh.toString());
            followupDao.save(followup);
            logger.info("follow up by {}", followup.getNama());

            TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);

            subscribe.setTahunAjaran(tahunAjaran);
            subscribe.setFollowup(followup);
            subscribe.setHubungi(0);
            subscribe.setSumber(utm_medium);
            subscribe.setKelas("Reguler");
            subscribe.setJenjang(Jenjang.S2);
            subscribeDao.save(subscribe);

            logger.info("Sukses : {} ", cekEmail);
            attributes.addFlashAttribute("selesai", "selesai");
            return "redirect:/selesaiPasca?email="+subscribe.getEmail();
        } else {
            logger.info("Gagal : {} ", cekEmail);
            return "redirect:/gagal?email="+subscribe.getEmail();
        }
    }
}
